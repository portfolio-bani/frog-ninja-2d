﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemCollection : MonoBehaviour
{
    private int pineapples=0; //to count total pineapples

    [SerializeField] private Text pineappleText;
    [SerializeField] private AudioSource collectSoundEffect;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Pineapple"))
        {   
            collectSoundEffect.Play();
            Destroy(collision.gameObject);
            pineapples++;
            pineappleText.text= ("Pineapples: " + pineapples);
        }
    }
}
