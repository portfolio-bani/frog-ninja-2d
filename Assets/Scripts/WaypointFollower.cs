﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollower : MonoBehaviour
{
    [SerializeField] private GameObject[] waypoints;
    private int currentWaypointIndex = 0;

    [SerializeField] private float speed = 2f;

    private void Update()
    {
        if (Vector2.Distance(waypoints[currentWaypointIndex].transform.position, transform.position) < .1f) //jeta first waypoint er distance and platfrom tar distance .1f er theke kom hole
        {
            currentWaypointIndex++; //pouche jacche then waypoint jog hocche
            if (currentWaypointIndex >= waypoints.Length) //length bolte total waypoint er soman jodi hoye jay current waypoint then waypoint abar zero te chole jacche
            { currentWaypointIndex = 0; }
        }
        transform.position = Vector2.MoveTowards(transform.position, waypoints[currentWaypointIndex].transform.position, Time.deltaTime * speed);
        //plateform er position= platform jacche .platfor position.waypoint position.speed
    }
}
